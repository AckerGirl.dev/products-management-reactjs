import React, { Component } from 'react'
import { Header } from "../header/Header";
export class AddProduct extends Component {
    constructor () {
        super();
        this.state = {
            name:'', namePlaceholder:"Nom du produit",
            category:'', categoryPlaceholder:"Categorie du produit",
            price:'', pricePlaceholder:"Prix du produit",
            description:'', descriptionPlaceholder:"Description",
            picture:'', picturePlaceholder:"Image du produit"
        }
    }

    handleNameoChange = event => this.setState({
        name:event.target.value
    });

    handleCategoryChange = event => this.setState({
        category:event.target.value
    });

    handlePriceChange = event => this.setState({
        price:event.target.value
    });

    handleDescriptionChange = event => this.setState({
        description:event.target.value
    });

    handlePictureChange = event => this.setState({
        picture:event.target.value
    });

    render() {
        return (
            <div>
            <Header/>
            <div className='container'>
                <form className='col-6 offset-sm-3 mt-5'>
                    <h3><label>Add Product</label></h3>
                    <div className='row'>
                        <div className='col-6'>
                            <input 
                                type="text" name='name'
                                className="form-control mb-1"
                                placeholder={this.state.namePlaceholder}
                                onChange={this.handleNameoChange}
                            />
                        </div>
                        <div className='col-6'>
                            <input 
                                type="text" name='category'
                                className="form-control mb-1"
                                placeholder={this.state.categoryPlaceholder}
                                onChange={this.handleCategoryChange}
                            />
                        </div>
                    </div>
                    <input 
                        type="number" name='price'
                        className="form-control mb-1"
                        placeholder={this.state.pricePlaceholder}
                        onChange={this.handlePriceChange}
                    />
                    <textarea
                        name='description'
                        className="form-control mb-1"
                        placeholder={this.state.descriptionPlaceholder}
                        onChange={this.handleDescriptionChange}
                    />
                    <input 
                        type="file" name='picture'
                        className="form-control mb-1"
                        placeholder={this.state.picturePlaceholder}
                        onChange={this.handlePictureChange}
                    />
                    <input type="submit" className='btn btn-primary mb-1' value= "New Product"/>
                </form>
            </div>
        </div>
        )
    }

    handleAdd = e => {
        e.preventDefault();

        this.props.handleAdd(
            this.state.name,
            this.state.category,
            this.state.price,
            this.state.description,
            this.state.picture
        );

        this.setState({name:''});
        this.setState({category:''});
        this.setState({price:''});
        this.setState({description:''});
        this.setState({picture:''});
    }

}

export default AddProduct

