import axios from 'axios';
import React, { Component } from 'react'
import AddProduct from '../addProduct/AddProduct';

export class Shop extends Component {
    constructor () {
        super();
        this.state = {
            isLoading: true, products: [], error:null
        }
    }

    componentDidMount () {
        axios.get('http://localhost:3333/products')
        .then(res => {
            const products = res.data;
            this.setState({ products, isLoading:false });
        })
        .catch(error => this.setState({ error, isLoading: false }))
    }

    render() {
        return (
            <div>
                <AddProduct handleAdd = {this.handleAdd}/>
            </div>
        )
    }

    handleAdd = (name, category, price, description, picture) => {
        axios.post('http://localhost:3333/products/', {name, category, price, description, picture})
        .then(res => {
            this.setState({ products : [...this.state.products, res.data] })
        })     
    }
}

export default Shop
