import React from 'react'

function Product({product}) {
    return (
        <div className = "centrer" style={{ textAlign:"center", borderBottom:"1px dotted", width:"200px"}}>
            { product.name }
            <button style={ {float:"right"}}>X</button>
        </div>
    )
}

export default Product